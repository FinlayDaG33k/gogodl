import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Time } from '../util/Time.util';
import { StatusHandler } from '../util/StatusHandler';
import { Notifier } from '../util/Notifier.util';
import { IUser } from '../util/interfaces/IUser';
import { Csrf } from '../util/Csrf.util';
import * as AppVars from '../util/AppVars';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  user: IUser = {
    loggedin: false
  };

  loginError = "";
  time = new Time();
  statusHandler = new StatusHandler();

  constructor(
    private httpClient: HttpClient,
    private notifier: Notifier,
    private csrf: Csrf
  ){
    this.csrf.update();
  }
  
  public async login(loginData) {
    // Add our form params
    let params = new HttpParams({
      fromObject: loginData
    });

    // Add our csrf token
    params = params.append(`_csrf`, this.csrf.getToken());

    // Send our request
    let resp = this.httpClient.post(
      `https://${AppVars.host}/login.html`, 
      params.toString(),
      {
        responseType: 'json',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        withCredentials: true
      }
    );

    return resp.toPromise();
  }

  /**
   * Obtain some details for this user
   * 
   * @param string html Optional HTML body which to use
   */
  public async getDetails(html: string|any = '') {
    // Get the html if we need to
    if(!html) {
      // Send our request
      let req = this.httpClient.get(
        `https://${AppVars.host}/`,
        {
          responseType: 'text',
          observe: 'response'
        }
      );
      
      // Turn our request into an awaitable promise
      // Also await it
      const resp = await (req.toPromise());

      // Check our status
      let status = AppVars.statusHandler.handle(resp);
      if(status.error) return { error: true, internal: status.internal, message: status.message };

      // Get our body as the html
      html = resp.body;
    }

    // Parse the Html
    html = AppVars.htmlParser.parseFromString(html, "text/html");

    // Check if we have an html
    if(!html) return;

    // Get the username
    const usernameElement = html.querySelector(`div.user_auth ul.auth li.user a.account`);
    
    // Check if we have a username
    // If so, set it
    if(!usernameElement) return;
    if(usernameElement.textContent) this.user.username = usernameElement.textContent;

    // Set out logged in status
    this.user.loggedin = true;

    // Return out status
    return { error: false };
  }

  public async logout() {
    this.user = {
      loggedin: false
    };

    /*
    let resp = await this.httpClient.get(
      `https://9anime.to/user/logout`   
    ).toPromise().catch(err => {});
    */
  }
}
