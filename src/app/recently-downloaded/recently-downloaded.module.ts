import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecentlyDownloadedRoutingModule } from './recently-downloaded-routing.module';

import { RecentlyDownloadedComponent } from './recently-downloaded.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [RecentlyDownloadedComponent],
  imports: [CommonModule, SharedModule, RecentlyDownloadedRoutingModule]
})
export class RecentlyDownloadedModule {}
