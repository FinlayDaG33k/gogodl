import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalService, ModalDirective, BsModalRef } from 'ngx-bootstrap/modal';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import * as AppVars from '../../../util/AppVars';

const { shell } = require('electron');

@Component({
  selector: 'app-update-modal',
  templateUrl: './update-modal.component.html',
  styleUrls: ['./update-modal.component.scss']
})

export class UpdateModalComponent implements OnInit {
  @ViewChild('updateModal', {static: true}) modal: any;
  modalRef: BsModalRef;
  faTimes = faTimes;

  localVersion;
  remoteVersion;
  description;
  changes;

  constructor(
    private modalService: BsModalService,
  ) {}

  ngOnInit() {}

  public openModal(options = {}) {
    this.localVersion = options['localVersion'];
    this.remoteVersion = options['remoteVersion'];
    this.description = options['description'];
    this.changes = options['changes'];

    this.modalRef = this.modalService.show(this.modal, {
      backdrop: true,
      class: 'modal-lg',
      ignoreBackdropClick: false
    });
  }

  public update() {
    shell.openExternal('https://www.finlaydag33k.nl/projects/3');
  }
}
