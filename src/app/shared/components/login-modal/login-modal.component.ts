import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BsModalService, ModalDirective, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder } from '@angular/forms';
import { faTimes, faUser } from '@fortawesome/free-solid-svg-icons';

import * as AppVars from '../../../util/AppVars';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})

export class LoginModalComponent implements OnInit {
  @ViewChild('loginModal', {static: true}) modal: any;
  modalRef: BsModalRef;
  loginForm;

  faTimes = faTimes;
  faUser = faUser;

  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {
    this.loginForm = this.formBuilder.group({
      email: '',
      password: ''
    });
  }

  ngOnInit() {
    
  }

  public openModal() {
    this.modalRef = this.modalService.show(this.modal, {
      backdrop: true,
      class: 'modal-lg',
      ignoreBackdropClick: false
    });
  }

  async onSubmit(loginData) {
    // Try to login
    let resp = await this.userService.login(loginData);
  
    // Check our status
    let status = AppVars.statusHandler.handle(resp);
    if(status.error) return { error: true, internal: status.internal, message: status.message };

    this.userService.loginError = "";
    this.userService.getDetails(resp['body']);
    this.modalRef.hide();
  }
}
