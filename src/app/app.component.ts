import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ElectronService } from './core/services';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import { Router } from '@angular/router';

import { GogoAnime } from './util/GogoAnime.util';
import { Cache } from './util/Cache.util';
import { faSearch, faTimes, faMinus, faMinusSquare, faPlusSquare, faCodeBranch, faDonate, faInfo, faCommentAlt, faUser } from '@fortawesome/free-solid-svg-icons';
import { faDiscord } from '@fortawesome/free-brands-svg-icons';
import { app } from 'electron';
import { DownloadTabService } from './download-tab.service';
import { UserService } from './services/user.service';
import { LoginModalComponent } from './shared/components/login-modal/login-modal.component';
import { UpdateModalComponent } from './shared/components/update-modal/update-modal.component';
import { Notifier } from './util/Notifier.util';
import { Logger } from './util/Logger.util';
import { GraphQL } from './util/GraphQL';

import { SharedModule } from './shared/shared.module';

const win = require('electron').remote.getCurrentWindow();
const { shell } = require('electron');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild(LoginModalComponent, {static: false}) private loginModalComponent:LoginModalComponent;
  @ViewChild(UpdateModalComponent, {static: false}) private updateModalComponent:UpdateModalComponent;
  logger = new Logger();
  searchTimeout = null;
  isBusy = true;
  nagDonation = false;
  isMaximized = win.isMaximized();
  downloads = [];
  updateAvailable: boolean = false;

  faSearch = faSearch;
  faTimes = faTimes;
  faMinus = faMinus;
  faMinusSquare = faMinusSquare;
  faPlusSquare = faPlusSquare;
  faCodeBranch = faCodeBranch;
  faDonate = faDonate;
  faInfo = faInfo;
  faCommentAlt = faCommentAlt;
  faUser = faUser;
  faDiscord = faDiscord;

  constructor(
    public electronService: ElectronService,
    private translate: TranslateService,
    public gogoAnime: GogoAnime,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    public downloadTabService: DownloadTabService,
    public userService: UserService,
    public sharedModule: SharedModule,
    public notifier: Notifier
  ) {
    translate.setDefaultLang('en');
     this.logger.info(`Starting GogoDl - version ${require('electron').remote.app.getVersion()}`);

    let self = this;
    win.on('maximize', () => {
      self.isMaximized = true;
      this.cdRef.detectChanges();
    });

    win.on('unmaximize', () => {
      self.isMaximized = false;
      this.cdRef.detectChanges();
    });

    self.downloads = this.downloadTabService.getAllDownloads();
  }

  ngOnInit() {
    this.userService.getDetails();

    // Check if there are any updates
    this.checkUpdates();
  }

  public async checkUpdates() {
    // Query the API
    const resp = await new GraphQL('https://www.finlaydag33k.nl/graphql')
      .setQuery(`
        query($projectId: Int!) {
          project(id: $projectId) {
            latest { 
              public {
                version
                description
                changelog {
                  change
                }
              }
            }
          }
        }
      `)
      .addVariable('projectId', 3)
      .execute();
    
    // If we don't have a response, return now
    if(!resp) return;

    // Get the latest version
    let remoteVersion: any = resp.project.latest.public.version;
    
    // Remove the period from the version
    // Do the same for the app version
    remoteVersion = remoteVersion.replace(/\./g, '');
    let localVersion: any = require('electron').remote.app.getVersion().replace(/\./g, '');

    // Turn our versions into numbers
    remoteVersion = parseInt(remoteVersion);
    localVersion = parseInt(localVersion);

    // Check if our current version is higher than our local version
    // Switch the flag if so
    this.updateAvailable = remoteVersion > localVersion;

    // Show the modal if our remote version is higher
    if(!this.updateAvailable) return;
    this.updateModalComponent.openModal({
      localVersion: require('electron').remote.app.getVersion(),
      remoteVersion: resp.project.latest.public.version,
      description: resp.project.latest.public.description,
      changes: resp.project.latest.public.changelog
    });
  }

  public openTab(index: number) {
    this.downloadTabService.openTab(index);
  }

  public searchChange(val) {
    // Clear the previous timeout
    clearTimeout(this.searchTimeout);

    // Set a new timeout
    this.searchTimeout = setTimeout(() => {
      // Show the loading page
      this.isBusy = true;

      // Make a quick search
      this.gogoAnime.searchAnime(val).then((res) => {
        this.router.navigate(['search'], { 
          state: { 
            series: res.series
          } 
        });
        this.isBusy = false;
      });
    }, 300);
  }

  public doDonate() {
    shell.openExternal('https://ko-fi.com/finlaydag33k');
  }

  public maximizeWindow() {
    win.maximize();
  }

  public unmaximizeWindow() {
    win.unmaximize();
  }

  public minimizeWindow() {
    win.minimize();
  }

  public exitApp() {
    // Remove cache before we exit
    let cache = new Cache();
    Object
      .entries(localStorage)
      .map(x => x[0])
      .filter(x => x.substring(0,15)=="discovery cache")
      .map(x => cache.remove(x));

    win.close();
  }

  public openRepo() {
    shell.openExternal('https://www.gitlab.com/finlaydag33k/gogoanime');
  }

  public sendFeedback() {
    shell.openExternal('https://www.reddit.com/r/finlaydag33k');
  }

  public joinDiscord() {
    shell.openExternal('https://discord.gg/fBg7KEC');
  }
}
