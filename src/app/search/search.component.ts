import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';

import { AppComponent } from '../app.component';
import { GogoAnime } from '../util/GogoAnime.util';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
  series: any;

  constructor(
    public gogoAnime: GogoAnime,
    private router: Router,
    public appComponent: AppComponent
  ) {
    try {
      this.series = this.router.getCurrentNavigation().extras.state.series;
    } catch(e) {
      this.appComponent.notifier.error("Oh boii", `Whoops, something went wrong while searching...\r\nSending you back to the discovery....`);
      this.router.navigate(['discover']);
    }
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.appComponent.isBusy = false;
  }
}
