import 'reflect-metadata';
import '../polyfills';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, Injectable, isDevMode } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

import { AppRoutingModule } from './app-routing.module';
import { AppConfig } from '../environments/environment';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { DiscoverModule } from './discover/discover.module';
import { SearchModule } from './search/search.module';
import { DownloadModule } from './download/download.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AboutModule } from './about/about.module';

import { AppComponent } from './app.component';
import { Logger } from './util/Logger.util';
import { Notifier } from './util/Notifier.util';

import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { WatchlistModule } from './watchlist/watchlist.module';
import { RecentlyDownloadedModule } from './recently-downloaded/recently-downloaded.module';

import * as Sentry from "@sentry/browser";
import { InterceptorService } from './services/interceptor.service';
import { GlobalHttpInterceptorService } from './services/GlobalHttpInterceptorService';

Sentry.init({
  dsn: "https://99a094b8b482424084ad544b642b44f3@o89247.ingest.sentry.io/5448952",
  release: `GogoDl@${require('electron').remote.app.getVersion()}`,
  enabled: AppConfig.production
});

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  logger = new Logger();

  constructor(
    private notifier: Notifier
  ) {}
  handleError(error) {
    const eventId = Sentry.captureException(error.originalError || error);
    this.logger.handleError(error);
    this.notifier.error("Oops...", `Something went wrong, we've already been notified!\r\nevent id: ${eventId}`);
  }
}


@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    DiscoverModule,
    SearchModule,
    DownloadModule,
    WatchlistModule,
    AppRoutingModule,
    AboutModule,
    RecentlyDownloadedModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    FontAwesomeModule,
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GlobalHttpInterceptorService,
      multi: true
    },
    {
      provide: ErrorHandler,
      useClass: SentryErrorHandler
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
