import { Time } from './Time.util';
import { Logger } from './Logger.util';
import { Cache } from './Cache.util';
import { StatusHandler } from './StatusHandler';

export const htmlParser = new DOMParser();
export const time = new Time();
export const logger = new Logger();
export const cache = new Cache();
export const servers = {
  "Gogo Server": 100
};
export const statusHandler = new StatusHandler();

export var host = "gogoanime.video";