import * as AppVars from './AppVars';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { getHttpContent } from './getHttpContent';
import { IInternalResponse } from './interfaces/IInternalResponse';

@Injectable({
  providedIn: 'root'
})

export class Csrf {
  private token: string = "";
  private getHttpContent = getHttpContent;

  constructor(
    private httpClient: HttpClient
  ) {}

  /**
   * Set the CSRF Token
   * 
   * @param string value The new CSRF token
   */
  public setToken(value: string): void {
    this.token = value;
  }

  /**
   * Get the token
   * 
   * @return string The current CSRF token
   */
  public getToken(): string {
    return this.token;
  }

  /**
   * Automatically updates the CSRF Token 
   * 
   * @return Promise<IInternalResponse>
   */
  public async update(): Promise<IInternalResponse> {
    // Get the homepage HTML
    const resp = await this.getHttpContent(`https://${AppVars.host}/`);

    // Check our status
    let status = AppVars.statusHandler.handle(resp);
    if(status.error) return { error: true, internal: status.internal, message: status.message };

    // Parse the HTML into a DOM object
    const html = AppVars.htmlParser.parseFromString(resp.body, "text/html");

    // Find the element which should hold our token
    const tokenElement = html.querySelector(`meta[name=csrf-token]`);

    // Check if a Csrf token is found
    // If not, return null
    // Else, update and return the token
    if(!tokenElement) return { error: true, internal: true, message: 'Could not obtain CSRF token' };
    if(!tokenElement.getAttribute("content")) return { error: true, internal: true, message: 'Could not obtain CSRF token' };
    this.token = tokenElement.getAttribute("content");
    return { error: false, data: tokenElement.getAttribute("content") };
  }
}