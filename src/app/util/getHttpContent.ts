/**
   * Get the Http content from a page
   * @param url 
   * @return Promise<any>
   */
export async function getHttpContent(url: string, opts = {}): Promise<any> {
    // Send our request
    let resp = this.httpClient.get(
      url, {
        responseType: opts['responseType'] || "text",
        observe: 'response'
      }
    );

      // Turn our request into a promise
    let prom = resp.toPromise();

    // Return our promise
    return prom;
}