import * as AppVars from './AppVars';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { getHttpContent } from './getHttpContent';
import { discoverAnime } from './gogolib/discoverAnime';
import { getAnimeId } from './gogolib/getAnimeId';
import { getEmbed } from './gogolib/getEmbed';
import { getEpisodesList } from './gogolib/getEpisodesList';
import { getServerName } from './gogolib/getServerName';
import { searchAnime } from './gogolib/searchAnime';
import { getDownloadPage } from './gogolib/getDownloadPage';

@Injectable({
  providedIn: 'root'
})

export class GogoAnime {
  constructor(
    private httpClient: HttpClient
  ) {}

  /**
   * All these are methods that will be split up into individual files.
   * This is done to keep this somewhat clean.  
   * You can find all of them in the "gogolib" folder.
   */
  public getHttpContent = getHttpContent;
  public discoverAnime = discoverAnime;
  public getAnimeId = getAnimeId;
  public getEmbed = getEmbed;
  public getEpisodesList = getEpisodesList;
  public getServerName = getServerName;
  public searchAnime = searchAnime;
  public getDownloadPage = getDownloadPage;
}