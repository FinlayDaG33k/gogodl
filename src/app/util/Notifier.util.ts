import { Injectable, isDevMode } from '@angular/core';
import { ToastrService, Toast } from 'ngx-toastr';

const electron = require('electron');
const win = electron.remote.getCurrentWindow();
const path = require('path');

@Injectable({
  providedIn: 'root'
})

export class Notifier {
  icon: string;

  constructor(
    private toastr: ToastrService
  ){}

  protected createNotification(title: string, message: string) {
    // Get the icon
    if(isDevMode()) {
      this.icon = path.join(electron.remote.app.getAppPath(), 'src/', 'favicon.png');
    } else {
      this.icon = path.join(electron.remote.process.execPath,'../', 'favicon.png');
    }

    // Check whether we are in focus
    if(!win.isFocused()) {
      // We are not in focus
      // Send the notification
      let myNotification = new Notification(title, {
        body: message,
        icon: this.icon
      });
    }
  }

  public info(title: string, message: string) {
    this.toastr.info(message, title);
    this.createNotification(title, message);
  }

  public error(title: string, message: string) {
    this.toastr.error(message, title);
    this.createNotification(title, message);
  }

  public success(title: string, message: string) {
    this.toastr.success(message, title);
    this.createNotification(title, message);
  }

  public warning(title: string, message: string) {
    this.toastr.warning(message, title);
    this.createNotification(title, message);
  }
}