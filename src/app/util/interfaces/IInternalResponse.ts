import { ISerie } from './ISerie';

export interface IInternalResponse {
  error: boolean;
  internal?: boolean;
  message?: string;
  data?: any;
  series?: ISerie[];
}