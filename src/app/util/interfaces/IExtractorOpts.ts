export interface IExtractorOpts {
  embed: string;
  server: string;
}