export interface ISerie {
  name: string;
  url: string;
  thumbnail: string;
  latest: string;
  status?: any;
}