export interface IUser {
  loggedin: boolean,
  username?: string,
}
