export interface IEpisode {
  episode: number;
  url: string;
  download: boolean;
}