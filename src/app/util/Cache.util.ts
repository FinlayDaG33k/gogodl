import { Injectable } from '@angular/core';
import { Logger } from './Logger.util';
import { Time } from './Time.util';
import { AppConfig } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class Cache {
  logger = new Logger();
  time = new Time();

  public set(name: string, data: any, expiry: number = 5) {
    this.logger.info(`Writing item "${name}" to cache`);
    localStorage.setItem(name, JSON.stringify({
      expiry: this.time.addMinutes(5),
      data: data
    }));
  }

  public get(name: string) {
    this.logger.info(`Retrieving item "${name}" from cache`);
    let cache = localStorage.getItem(name);
    if(cache) {
      return JSON.parse(cache).data;
    }

    return null;
  }

  public remove(name: string) {
    localStorage.removeItem(name);
  }

  public check(name: string) {
    // While not in production, always return false
    if(!AppConfig.production) {
      this.logger.info(`App found to be in not in production mode, bypassing cache for "${name}"!`);
      return false;
    }

    // Get the item from the cache
    const cache = localStorage.getItem(name);

    // Check if a cache item was found
    if(!cache) {
      this.logger.info(`Cache item "${name}" could not be found!`);
      return false;
    }

    // Check if we can parse the cache
    let item;
    try {
      item = JSON.parse(cache);
    } catch(e) {
      this.logger.error(`Could not parse cache item "${name}!`);
      return false;
    }

    // Check if cache has expired
    if(item.expiry <= this.time.getUnixTimestamp()) {
      this.logger.info(`Cache item "${name}" has expired`);
      return false;
    }

    // Check if the cache item has any data in it
    if(item.data.length == 0) {
      this.logger.warning(`Cache item "${name}" does not contain any data`);
      return false;
    }

    // Everything is still right
    this.logger.debug(`Cache item "${name}" is still good`);
    return true;
  }
}