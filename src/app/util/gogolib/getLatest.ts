/**
 * Extract the latest episode from a serie
 * This function may be moved later if proven re-usable
 *  
 * @param Object The element (DOM node) to look into
 * @return string The latest episode
 */
export function getLatest(element): string {
  // Get the element that should hold the episode
  const episodeElement = element.querySelector(`p.episode`);

  // Check if the episodeElement was found
  // If not, return questionmarks
  // Else, return its text content
  if(!episodeElement) return "???";
  if(!episodeElement.textContent) return "???";
  return episodeElement.textContent;
}