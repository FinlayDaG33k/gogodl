import { IEpisode } from "../interfaces/IEpisode";

/**
 * Parse a DOM node to obtain a list of all episodes for this anime
 * 
 * @param element element The element (DOM node) which to parse 
 */
export function getEpisodesList(element): IEpisode[] {
  // Create a variable to hold our episodes
  let episodes: IEpisode[] = [];

  // Get a list of all episodes
  const episodesElements = element.querySelectorAll(`ul#episode_related li a`);

  // Loop over each element found
  episodesElements.forEach((episode) => {
    // Create our data object
    const data = {
      episode: getEpisodeNumber(episode),
      url: getEpisodeUrl(episode).trim(),
      download: true
    };

    // Add this episode to the list
    episodes.push(data);
  });

  return episodes;
}

/**
 * Extract the episode number from an element
 * 
 * @param element element The element (DOM node) which to search for
 * @return number|null The number of the episode
 */
function getEpisodeNumber(element): number|null {
  // Get the element that should hold our episode number
  const numberElement = element.querySelector(`a div.name`);

  // Check if an numberElement was found
  // If not, return null
  // Else, strip the "ep" prefix and return the result
  if(!numberElement) return null;
  if(!numberElement.textContent) return null;
  return numberElement.textContent.replace(`EP `, ``);
}

/**
 * Extract the episode url from an element
 * 
 * @param element element The element (DOM node) which to search for
 * @return string|null The url of the episode
 */
function getEpisodeUrl(element): string|null {
  // Check if an href was found
  // If not, return null
  // Else, return the href content
  if(!element.getAttribute(`href`)) return null;
  return element.getAttribute(`href`);
}