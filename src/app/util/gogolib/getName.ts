/**
 * Extract the name from a serie
 * 
 * @param element The element (DOM node) to look into
 * @return string The name of the serie
 */
export function getName(element): string {
  // Get the element which should hold the name
  const nameElement = element.querySelector(`p.name a`);

  // Check if a nameElement was found
  // If not, return questionmarks
  // Else return it's text content
  if(!nameElement) return "???";
  if(!nameElement.textContent) return "???";
  return nameElement.textContent;
}