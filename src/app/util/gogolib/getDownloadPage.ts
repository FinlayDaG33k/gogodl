import * as AppVars from '../AppVars';
import { IInternalResponse } from '../interfaces/IInternalResponse';

/**
 * Extract the download page url for an episode
 * 
 * @param episodeUrl string The url for the episode's main page 
 */
export async function getDownloadPage(episodeUrl: string): Promise<IInternalResponse> {
  // Build the url where to get the episode
  const url = `https://${AppVars.host}${episodeUrl}`;
  AppVars.logger.debug(`Getting download page from: ${url}`);

  // Get our page content
  const resp = await this.getHttpContent(url);

  // Check our status
  let status = AppVars.statusHandler.handle(resp);
  if(status.error) return { error: true, internal: status.internal, message: status.message};

  // Get the download page url
  const html = AppVars.htmlParser.parseFromString(resp.body, "text/html");
  const downloadElement = html.querySelector(`li.dowloads a`);
  
  // Check if an downloadElement was found
  // If not, return an error
  // Else, return the download url
  if(!downloadElement) return { error: true, internal: true, message: `Could not extract download page URL`};
  if(!downloadElement.getAttribute(`href`)) return { error: true, internal: true, message: `Could not extract download page URL`};
  return { error: false, data: downloadElement.getAttribute(`href`)};
}