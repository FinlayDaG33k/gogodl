/**
 * Extract the url from a serie
 * This function may be moves later if proven re-usable
 * 
 * @param element The Element (DOM node) to look into
 * @return string|null The URL of the serie
 */
export function getUrl(element): string|null {
  // Get the element which should hold the url
  const urlElement = element.querySelector(`p.name a`);

  // Check if a urlElement was found
  // If not, return null
  // Else, return its href attribute
  if(!urlElement) return null;
  if(!urlElement.getAttribute(`href`)) return null;
  return urlElement.getAttribute(`href`);
}