import * as AppVars from '../AppVars';
import { IEmbedOpts } from '../interfaces/IEmbedOpts';
import { IInternalResponse } from '../interfaces/IInternalResponse';

/**
 * Extract the embed URL for an episode
 * 
 * @param IEmbedOpts 
 */
export async function getEmbed(opts: IEmbedOpts): Promise<IInternalResponse> {
  // Build the url where to get the episode
  const url = `https://${AppVars.host}${opts.url}`;
  AppVars.logger.debug(`Getting embed from: ${url}`);

  // Get our page content
  const resp = await this.getHttpContent(url);

  // Check our status
  let status = AppVars.statusHandler.handle(resp);
  if(status.error) return { error: true, internal: status.internal, message: status.message};

  // Get the embed url
  const html = AppVars.htmlParser.parseFromString(resp.body, "text/html");
  const embedElement = html.querySelector(`a[data-video][rel="${opts.server}"]`);
  
  // Check if an embedElement was found
  // If not, return an error
  // Else, return the embed
  if(!embedElement) return { error: true, internal: true, message: `Could not extract embed URL`};
  if(!embedElement.getAttribute(`data-video`)) return { error: true, internal: true, message: `Could not extract embed URL`};
  return { error: false, data: `https:${embedElement.getAttribute(`data-video`)}`};
}