import * as AppVars from '../AppVars';
import { ISerie } from '../interfaces/ISerie';
import { IInternalResponse } from '../interfaces/IInternalResponse';
import { getLatest } from './getLatest';
import { getName } from './getName';
import { getThumbnail } from './getThumbnail';
import { getUrl } from './getUrl';

/**
 * Search for an anime
 * 
 * @param string query The query which to search for
 * @return Promise<IInternalResponse>
 */
export async function searchAnime(query: string): Promise<IInternalResponse> {
  // Create a new array
  let seriesData: ISerie[] = [];

  // Check if we have a cached version
  // If so, return that
  if(AppVars.cache.check(`search cache query=${query}`)) {
    let cache = AppVars.cache.get(`search cache query=${query}`)
    if(!cache) return { error: true, internal: true, message: `Could not obtain cache item "search cache query=${query}"`};
    return { error: false, series: cache };
  }

  // Get the content of the homepage
  let res = await this.getHttpContent(`https://${AppVars.host}/search.html?keyword=${query}`);

  // Check our status
  let status = AppVars.statusHandler.handle(res);
  if(status.error) return { error: true, internal: status.internal, message: status.message};

  // Turn the content into a DOM node
  var html = AppVars.htmlParser.parseFromString(res.body, "text/html");

  // Extract all the series from the DOM node
  let series = html.querySelectorAll("ul.items li");

  // Loop over all the series
  series.forEach(serie => {
    // Create our data object
    const data: ISerie = {
      name: getName(serie),
      url: getUrl(serie),
      thumbnail: getThumbnail(serie),
      latest: getLatest(serie),
      status: null
    };

    // Push our data object into the seriesData
    seriesData.push(data);
  });

  // Update our cache
  AppVars.cache.set(`search cache query=${query}`, seriesData);

  // Return our series
  return { error: false, series: seriesData };
}