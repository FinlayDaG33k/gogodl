import * as AppVars from '../AppVars';

/**
 * Get the name of a server by it's ID
 * @param id 
 */
export function getServerName(id: number): string {
  return Object.keys(AppVars.servers).find(key => AppVars.servers[key] === id);
}