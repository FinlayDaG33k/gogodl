/**
 * Extract the thumbnail from a serie
 * This function may be moved later if proven re-usable
 * 
 * @param element The element (DOM node) to look into
 * @return string|null The URL for the thumbnail
 */
export function getThumbnail(element): string|null {
  // Get the element which should hold the thumbnail
  const thumbnailElement = element.querySelector(`div.img a img`);
  
  // Check if a thumbnailElement was found
  // If not, return null
  // Else, return its src attribute
  if(!thumbnailElement) return null;
  if(!thumbnailElement.getAttribute(`src`)) return null;
  return thumbnailElement.getAttribute(`src`);
}