import * as AppVars from '../AppVars';
import { IInternalResponse } from '../interfaces/IInternalResponse';

/**
 * Get the "hidden" id for the anime
 * This is later used to get the episodes
 * 
 * @param string url The url of the page to look on
 */
export async function getAnimeId(url: string): Promise<IInternalResponse> {
  // Get the content of the series page
  const res = await this.getHttpContent(url);

   // Check our status
   let status = AppVars.statusHandler.handle(res);
   if(status.error) return { error: true, internal: status.internal, message: status.message};

  // Turn the content into a DOM node
  const html = AppVars.htmlParser.parseFromString(res.body, "text/html");

  // Get the element that should contain the id
  const idElement = html.querySelector(`input[type="hidden"][id="movie_id"].movie_id`);

  // Check if an idElement was found
  // Return data corresponding to the status
  if(!idElement) return { error: true, internal: true, message: `Could not obtain id!`};
  if(!idElement.getAttribute("value")) return { error: true, internal: true, message: `Could not obtain id!`};
  return { error: false, data: idElement.getAttribute("value") };
}