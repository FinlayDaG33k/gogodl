import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as AppVars from '../util/AppVars';
import { IExtractorOpts } from '../util/interfaces/IExtractorOpts';

import { GogoServerExtractor } from './extractors/gogoserver';

@Injectable({
  providedIn: 'root'
})

export class Extractor {
  classes = {
    GogoServerExtractor
  }

  constructor(
    private httpClient: HttpClient
  ) {}

  public async extract(opts: IExtractorOpts) {
    // Build our className
    // - Strip spaces
    let className = opts.server;
    className = opts.server.replace(" ", "");

    // Instanciate the class
    let extractorClass = new this.classes[`${className}Extractor`](this.httpClient);

    // Extract the url
    try {
      let video = await extractorClass.doExtract(opts.embed);
      return video;
    } catch (e) {
      return null;
    }
  }
}