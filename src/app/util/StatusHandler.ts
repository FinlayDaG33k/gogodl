export class StatusHandler {
  STATUS_UNKNOWN: number = 0;
  STATUS_OK: number = 200;
  STATUS_NOT_FOUND: number = 404;
  STATUS_BAD_GATEWAY: number = 502;
  STATUS_SERVICE_UNAVAILABLE: number = 503;
  STATUS_GATEWAY_TIMEOUT: number = 504;
  STATUS_TIMEOUT: number = 524;

  MESSAGE_UNKNOWN: string = 'An unknown error occurred';
  MESSAGE_OK: string = 'Everything OK';
  MESSAGE_NOT_FOUND: string = 'The content was not found on 9Anime';
  MESSAGE_BAD_GATEWAY: string = 'Bad gateway error';
  MESSAGE_SERVICE_UNAVAILABLE: string = '9Anime is temporarily unavailable';
  MESSAGE_GATEWAY_TIMEOUT: string = '9Anime is temporarily unavailable';
  MESSAGE_TIMEOUT: string = 'Could not connect to 9Anime';

  public handle(data: any) {
    // Check if a status was set
    if(!data.hasOwnProperty("status")) return {error: true, internal: true, message: this.MESSAGE_UNKNOWN};

    // Check our status
    switch(data.status) {
      case this.STATUS_UNKNOWN:
        return {error: true, internal: false, message: this.MESSAGE_UNKNOWN};
      case this.STATUS_OK:
        return {error: false, internal: false, message: this.MESSAGE_OK};
      case this.STATUS_NOT_FOUND:
        return {error: true, internal: false, message: this.MESSAGE_NOT_FOUND}
      case this.STATUS_BAD_GATEWAY:
        return {error: true, internal: false, message: this.MESSAGE_BAD_GATEWAY};
      case this.STATUS_SERVICE_UNAVAILABLE:
        return {error: true, internal: false, message: this.MESSAGE_SERVICE_UNAVAILABLE};
      case this.STATUS_GATEWAY_TIMEOUT:
        return {error: true, internal: false, message: this.MESSAGE_GATEWAY_TIMEOUT};
      case this.STATUS_TIMEOUT:
        return {error: true, internal: false, message: this.MESSAGE_TIMEOUT};
      default:
        return {error: true, internal: true, message: this.MESSAGE_UNKNOWN};
    }
  }
}