import { ErrorHandler, Injectable, isDevMode } from '@angular/core';

const fs = require('fs');
const moment = require('moment');
const electron = require('electron');
const path = require('path');

@Injectable({
  providedIn: 'root'
})

export class Logger implements ErrorHandler{
  logdir;

  public log(message: string) {
    // Write to the regular JS Console
    console.log(message);

    // Write to the logfile
    this.writeOut(message);
  }

  public info(message: string) {
    // Write to the regular JS Console
    console.log(`[INFO] ${message}`);

    // Write to the logfile
    this.writeOut(`[INFO] ${message}`);
  }

  public debug(message: string) {
    // Write to the regular JS Console
    console.log(`[DEBUG] ${message}`);

    // Write to the logfile
    this.writeOut(`[DEBUG] ${message}`);
  }

  public error(message: string) {
    // Write to the regular JS Console
    console.error(`[ERROR] ${message}`);

    // Write to the logfile
    this.writeOut(`[ERROR] ${message}`);
  }

  public warning(message: string) {
    // Write to the regular JS Console
    console.warn(`[WARNING] ${message}`);

    // Write to the logfile
    this.writeOut(`[WARNING] ${message}`);
  }

  public writeOut(message: string) {
    // Set the path to the logdir
    if(isDevMode()) {
      this.logdir = path.join(electron.remote.app.getAppPath(), 'logs');
    } else {
      this.logdir = path.join(electron.remote.process.execPath,'../', 'logs');
    }

    // Create the output for the logfile
    let now = moment();
    let output = `[${now.format('YYYY/MM/DD HH:mm:ss')}] ${message}\r\n`;

    // Check if the logdir exists
    // Create it if needed
    if(!fs.existsSync(this.logdir)) {
      fs.mkdirSync(this.logdir, { recursive: true });
    }

    // Write to the logfile
    let file = path.join(this.logdir, `log-${now.format('YYYY-MM-DD')}.txt`);
    fs.appendFileSync(file, output);
  }

  handleError(error: Error) {
    // Create the output
    let output = `It appears something went wrong here:\r\n${error.message}\r\n${error.stack}`;

    // Write to the regular JS Console
    console.error(output);

    // Write to the logfile
    this.writeOut(output);
  }
}