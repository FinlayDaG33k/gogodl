import * as AppVars from '../AppVars';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { getHttpContent } from '../getHttpContent';

@Injectable({
  providedIn: 'root'
})

export class GogoServerExtractor {
  private getHttpContent = getHttpContent;

  constructor(
    private httpClient: HttpClient
  ) {}

  public async doExtract(embed) {
    // Get the content of the page
    const resp = await this.getHttpContent(embed);

    // Check our status
    let status = AppVars.statusHandler.handle(resp);
    if(status.error) return null;

    // Parse the HTML
    const html = AppVars.htmlParser.parseFromString(resp.body, "text/html");

    // Find the download element for the best quality
    const downloadElement = await this.findBestQuality(html, ['HD', '1080', '720']);

    // Check if a video element was found
    // Then check 
    // Return the video url if found
    if(!downloadElement) return null;
    if(!downloadElement.getAttribute('href')) return null;
    return downloadElement.getAttribute('href');
  }

  private findBestQuality(html, qualities: string[]): Promise<any> {
    return new Promise((resolve, reject) => {
      // Loop over each given quality
      // Try to find an element matching the quality
      // Return the first in line
      for(let quality of qualities) {
        // Find an element for the given quality
        AppVars.logger.debug(`Trying to find download for quality ${quality}p.`);
        const downloadElement = Array.from(html.querySelectorAll(`div.dowload a`))
          .filter(candidate => candidate['textContent'].includes(`(${quality}P - mp4)`))[0];

        // Check if an element was found
        if(!downloadElement) {
          AppVars.logger.debug(`could not find download for quality ${quality}p.`);
          continue;
        }

        // Element was found
        // Resolve the promise
        resolve(downloadElement);
      }
      reject(null);
    });
  }
}